import React, {Component, Fragment} from 'react';
import {StyleSheet, FlatList, Text, View, Button, Modal, TouchableHighlight} from 'react-native';
import {connect} from 'react-redux';

import ListItem from '../../components/ListItem/ListItem';
import {getList, addToBasket, addOrder} from "../../store/action";

class List extends Component {
    state = {
        modalVisible: false
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    componentDidMount() {
        this.props.getList();
    }

    render() {
        const order = {};
        this.props.basketList.map(item => {
           order[item.id] = item.amount;
        });


        if (this.props.loading) {
            return (
                <View style={styles.loading}>
                    <Text>Loading...</Text>
                </View>
            )
        } else {
            return (
                <View style={styles.container}>
                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                        alert('Modal has been closed.');
                    }}>
                        <View style={{marginTop: 22}}>
                            <View>
                                <Text>You order:</Text>
                                {this.props.basketList.map(item => {
                                    return (
                                        <View key={item.title}>
                                            <Text>{item.title} x{item.amount} {item.price} KGS</Text>
                                        </View>
                                    )
                                })}

                                <Text>Delivery: 150 KGS</Text>
                                <Text>Total: {this.props.sum + 150}</Text>
                                <Button
                                    onPress={() => {this.setModalVisible(!this.state.modalVisible);}}
                                    title="Cancel"
                                    color="#841584"
                                    accessibilityLabel="Learn more about this purple button"
                                />
                                <Button
                                    onPress={() => this.props.addOrder(order)}
                                    title="Order"
                                    color="#841584"
                                    accessibilityLabel="Learn more about this purple button"
                                />
                            </View>
                        </View>
                    </Modal>
                    <View>
                        <Text>Papa Pizza</Text>
                    </View>
                    <FlatList
                        style={styles.list}
                        data={this.props.arrayList}
                        keyExtractor={(item) => item.id}
                        renderItem={(info) => (
                        <ListItem
                            addToBasket={() => this.props.addToBasket({title: info.item.title, price: info.item.price, id: info.item.id})}
                            title={info.item.title}
                            img={info.item.image}
                            price={info.item.price}
                        />
                    )}
                    />
                    <View>
                        <Text>Order total: {this.props.sum} KGS</Text>
                        <Button
                            onPress={() => {this.setModalVisible(true);}}
                            title="Checkout"
                            color="#841584"
                            accessibilityLabel="Learn more about this purple button"
                        />
                    </View>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        paddingVertical: 30
    },
    loading: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    }
});

const mapToProps = state => {
    return {
        arrayList: state.arrayList,
        loading: state.loading,
        basketList: state.basketList,
        sum: state.sum
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getList: () => dispatch(getList()),
        addToBasket: (order) => dispatch(addToBasket(order)),
        addOrder: (order) => dispatch(addOrder(order))
    }
};

export default connect(mapToProps, mapDispatchToProps)(List);