import React from 'react';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';

import reducer from './store/reducer';
import List from './containers/List/List';

const store = createStore(reducer, applyMiddleware(thunk));

const App = props => (
    <Provider store={store}>
        <List/>
    </Provider>
);

export default App;
