import {LIST_SUCCESS, LIST_REQUEST, LIST_ERROR, ADD_TO_BASKET, ORDER_SUCCESS} from "./action";

const initialState = {

    arrayList: [],
    loading: false,
    basketList: [],
    sum: 0
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case LIST_REQUEST:
            return {
                ...state,
                loading: true
            };
        case LIST_SUCCESS:
            const dishes = [];
            for (let key in action.listData) {
                dishes.push({...action.listData[key], id: key})
            }

            return {
                ...state,
                arrayList: dishes,
                loading: false
            };
        case LIST_ERROR:
            return {
                ...state,
                loading: false
            };
        case ADD_TO_BASKET:
            let basketList = [...state.basketList];

            if(basketList.length === 0) {
                basketList.push({...action.order, amount: 1})
            } else {
                let hasItem = true;

                for (let i = 0; i < basketList.length; i++) {
                    const currentItem = basketList[i];

                    if(currentItem.title === action.order.title) {
                        currentItem.amount++;
                        basketList[i] = currentItem;
                        hasItem = false;
                    }

                }

                if(hasItem) {
                    basketList.push({...action.order, amount: 1})
                }
            }

            return {...state, basketList: basketList, sum: state.sum += parseInt(action.order.price, 10)};
        case ORDER_SUCCESS:
            return {...state, basketList: [], sum: 0};
        default:
            return state;
    }
};

export default reducer;