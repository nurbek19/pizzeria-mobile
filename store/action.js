import axios from '../axios-dishes';

export const LIST_REQUEST = 'LIST_REQUEST';
export const LIST_SUCCESS = 'LIST_SUCCESS';
export const LIST_ERROR = 'LIST_ERROR';

export const ORDER_REQUEST = 'ORDER_REQUEST';
export const ORDER_SUCCESS = 'ORDER_SUCCESS';
export const ORDER_ERROR = 'ORDER_ERROR';

export const ADD_TO_BASKET = 'ADD_TO_BASKET';

export const listRequest = () => {
  return {type: LIST_REQUEST}
};

export const listSuccess = (listData) => {
  return {type: LIST_SUCCESS, listData}
};

export const listError = () => {
    return {type: LIST_ERROR}
};

export const orderRequest = () => {
  return {type: ORDER_REQUEST}
};

export const orderSuccess = () => {
  return {type: ORDER_SUCCESS}
};

export const orderError = () => {
    return {type: ORDER_ERROR}
};

export const addToBasket = order => {
    return {type: ADD_TO_BASKET, order}
};

export const getList = () => {
    return dispatch => {
        dispatch(listRequest());
        axios.get('foods.json').then(response => {
            dispatch(listSuccess(response.data));
        }, error => {
            dispatch(listError());
        })
    }
};

export const addOrder = (order) => {
    return dispatch => {
        dispatch(orderRequest());
        axios.post('dishes-orders.json', order).then(() => {
            dispatch(orderSuccess());
        }, error => {
            dispatch(orderError());
        })
    }
};
