import React from 'react';
import {StyleSheet, TouchableOpacity, Text, View, Image} from 'react-native';

const ListItem = props => {
    return (
        <TouchableOpacity style={styles.listItem} onPress={props.addToBasket}>
            <View style={{width: '80%'}}>
                <Image style={{width: 50, height: 50, marginRight: 10}} source={{uri: props.img}}/>
                <Text>{props.title}</Text>
            </View>
            <Text>{props.price} KGS</Text>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    listItem: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#eee',
        width: '100%',
        marginBottom: 10,
        padding: 10
    },
});

export default ListItem;