import axios from 'axios';

const instance = axios.create({
   baseURL: 'https://burger-template-2faae.firebaseio.com'
});

export default instance;
